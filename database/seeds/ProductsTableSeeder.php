<?php

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new Product();
        $product->name = 'Samsung Gallaxy J7';
        $product->default_sell_price = 26800;
        $product->stock_quantity = 5;
        $product->slug = 'Samsung-Gallaxy-J7';
        $product->category_id = 1;
        $product->brand_id = 1;
        $product->save();

        $product = new Product();
        $product->name = 'Iphone X10';
        $product->default_sell_price = 75000;
        $product->stock_quantity = 2;
        $product->slug = 'Iphone-X10';
        $product->category_id = 1;
        $product->brand_id = 2;
        $product->save();
    }
}
