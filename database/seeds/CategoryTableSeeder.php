<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new Category();
        $category->name = 'Mobile Phone';
        $category->slug = 'mobile-phone';
        $category->save();

        $category = new Category();
        $category->name = 'Laptop';
        $category->slug = 'laptop';
        $category->save();
    }
}
