<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug')->unique();
            
            $table->float('default_sell_price')->default(0.0);
            $table->unsignedInteger('stock_quantity')->default(0);

            $table->string('image')->nullable();
            $table->string('sku')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('brand_id');

            $table->foreign('category_id')->references('id')
            ->on('categories')->onDelete('cascade');
            
            $table->foreign('brand_id')->references('id')
            ->on('brands')->onDelete('cascade');

            $table->softDeletesTz(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
