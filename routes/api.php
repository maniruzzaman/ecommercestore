<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'products'], function () {
    Route::get('/', 'API\ProductController@index');
    Route::get('/search/{search}', 'API\ProductController@search');
    Route::get('/view/{id}', 'API\ProductController@show');
    Route::post('/', 'API\ProductController@store');
    Route::put('/update/{id}', 'API\ProductController@update');
    Route::delete('/delete/{id}', 'API\ProductController@delete');
});

Route::group(['prefix' => 'brands'], function () {
    Route::get('/', 'API\BrandController@index');
    Route::get('/view/{id}', 'API\BrandController@show');
    Route::post('/', 'API\BrandController@store');
    Route::put('/update/{id}', 'API\BrandController@update');
    Route::delete('/delete/{id}', 'API\BrandController@delete');
});


Route::group(['prefix' => 'categories'], function () {
    Route::get('/', 'API\CategoryController@index');
    Route::get('/view/{id}', 'API\CategoryController@show');
    Route::post('/', 'API\CategoryController@store');
    Route::put('/update/{id}', 'API\CategoryController@update');
    Route::delete('/delete/{id}', 'API\CategoryController@delete');
});