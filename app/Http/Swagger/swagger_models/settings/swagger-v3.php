<?php
/**
 * @OA\Info(
 *     description="API Documentation - My Ecommerce Store",
 *     version="1.0.0",
 *     title="Online Ecommerce Store Swagger API Documentation",
 *     @OA\Contact(
 *         email="manirujjamanakash@gmail.com"
 *     ),
 *     @OA\License(
 *         name="@Digital Ecommerce Store",
 *         url="http://test.com"
 *     )
 * )
 */