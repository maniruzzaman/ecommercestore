<?php

namespace App\Http\Controllers\API;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * @OA\GET(
     *     path="/api/products",
     *     tags={"Products"},
     *     summary="All products",
     *     description="Returns the all list of products",
     *     operationId="sampleFunctionWithDoc",
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function index()
    {
        $products = Product::orderBy('id', 'desc')
                    ->with('category', 'brand')
                    ->get();
        return $products;
    }


    /**
     * @OA\GET(
     *     path="/api/products/search/{search}",
     *     tags={"Products"},
     *     summary="Search Products",
     *     description="Returns Searched Result of Products",
     * @OA\Parameter(
     *          name="search",
     *          description="Product ID or title or SKU",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     operationId="sampleFunctionWithDoc",
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function search($search)
    {
        $products = Product::orderBy('id', 'desc')
                    ->where('name', 'like', '%'.$search.'%')
                    ->orWhere('id', 'like', '%'.$search.'%')
                    ->orWhere('sku', 'like', '%'.$search.'%')
                    ->with('category', 'brand')
                    ->get();
        return $products;
    }


    /**
     * @OA\POST(
     *     path="/api/products",
     *     tags={"Products"},
     *     summary="Store Product",
     *     description="Store New Product and Gives the response",
     * @OA\Parameter(
     *          name="id",
     *          description="Product id, Ex: 10 (Must be auto increment and unique)",
     *          required=false,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Parameter(
     *          name="name",
     *          description="Product Name, eg; Sony X2",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     * @OA\Parameter(
     *          name="default_sell_price",
     *          description="Product Default Sell Price, eg; 1000.0",
     *          required=false,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     operationId="sampleFunctionWithDoc",
     *      @OA\Response(
     *          response=200,
     *          description="Product Stored Response"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @OA\GET(
     *     path="/api/products/view/{id}",
     *     tags={"Products"},
     *     summary="Details of a product",
     *     description="Get the details of a product",
     * @OA\Parameter(
     *          name="id",
     *          description="Product id, Ex: 10",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     operationId="sampleFunctionWithDoc",
     *      @OA\Response(
     *          response=200,
     *          description="Product Found Response"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id)
    {
        $product = Product::where('id', $id)->with('category', 'brand')->first();
        return $product;
    }

/**
     * @OA\PUT(
     *     path="/api/products/update/{id}",
     *     tags={"Products"},
     *     summary="Returns a Sample API response",
     *     description="Gives us the all list of products",
     *     operationId="sampleFunctionWithDoc",
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * @OA\DELETE(
     *     path="/api/products/delete/{id}",
     *     tags={"Products"},
     *     summary="Returns a Sample API response",
     *     description="Gives us the all list of products",
     *     operationId="sampleFunctionWithDoc",
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function destroy(Product $product)
    {
        //
    }
}
